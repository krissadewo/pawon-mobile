/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$.host = "http://192.168.155.1:9666/pawon";
$.imgHost = "http://192.168.155.1:9666/media/";
$.merchantName = "Pawon Resto";
$.checkHostTimeOut = 50000;

$(document).bind("mobileinit", function() {
    $.mobile.ajaxEnabled = false;
    $.mobile.defaultPageTransition = 'slide';
    $.mobile.phonegapNavigationEnabled = true;


    // Navigation
//    $.mobile.page.prototype.options.backBtnText = "Go back";
//    $.mobile.page.prototype.options.addBackBtn = true;
    // $.mobile.page.prototype.options.backBtnTheme = "d";


    // Page
    $.mobile.page.prototype.options.headerTheme = "c";  // Page header only
    //$.mobile.page.prototype.options.contentTheme = "c";
    $.mobile.page.prototype.options.footerTheme = "c";

    // Listviews
    $.mobile.listview.prototype.options.headerTheme = "c";  // Header for nested lists
    $.mobile.listview.prototype.options.theme = "c";  // List items / content
    $.mobile.listview.prototype.options.dividerTheme = "c";  // List divider

    $.mobile.listview.prototype.options.splitTheme = "c";
    $.mobile.listview.prototype.options.countTheme = "c";
    $.mobile.listview.prototype.options.filterTheme = "c";
    $.mobile.listview.prototype.options.filterPlaceholder = "Filter data...";


    $.mobile.loader.prototype.options.text = "loading";
    $.mobile.loader.prototype.options.textVisible = true;
    $.mobile.loader.prototype.options.theme = "c";
    $.mobile.loader.prototype.options.html = "";
});

function checkServer() {

    window.setInterval(function() {
        $.ajax({
            cache: false,
            type: 'get',
            dataType: "jsonp",
            url: $.host + "/language.jsonp",
            timeout: 5000,
            error: function() {
                alert("eerror");
            },
            success: function() {
                window.location.href = "index.html";
            }
        });

    }, 2000);
}

$.getParam = function(name) {
    var results = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    if (results !== null) {
        return results[1] || 0;
    }
    return null;
};

$.getJsonp = function(pUrl, successCalback) {
    var response = $.ajax({
        url: pUrl,
        type: "get",
        dataType: "jsonp",
        success: successCalback,
        timeout: 5000,
        cache: false,
        error: function(data, status) {
            $.mobile.loading('show', {theme: "b", text: "Please try again, network busy...", textonly: true});
        }       
    });

    return response;
};

function supportWebStroge() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

$.formatPrice = function(price) {
    return format("#,##0.####", price);
};

$.reloadPage = function(url) {
    jQuery.mobile.changePage(url, {
        transition: 'slide',
        reloadPage: true,
        ajax: false
    });
};
